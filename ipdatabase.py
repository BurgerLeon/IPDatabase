# -*- coding: utf-8 -*-
'''
Filename: main.py
Author: Leon Burger
Date: 06.01.2021
Python Version: 3.9.1
Dependencies: 
    ipdatabase -> database in wich the IPs are stored
    argparse -> parser for the arguments given by the user
    os -> provides access to system filesystem 
    netaddr -> validate IPs
    socket -> convert hostnames to IPs
    dpkt -> ethernet package parser
    multiprocessing -> parallel processing for reverse DNS
'''

from ipdatabase_backend import IPDatabase
from argparse import ArgumentParser, Namespace
from os import path
from netaddr import IPAddress, AddrFormatError
from socket import gethostbyaddr, gethostbyname, gaierror, herror, inet_ntop, AF_INET, AF_INET6
from dpkt import pcap, ethernet
from multiprocessing import Pool

class Interface:
    '''
    Description:
        class_description
    
    Methods:
        start(): Initiates the programm.
    '''

    def __init__(self) -> None:
        '''
        Description:
            Initiates the interface. Checks if a valid database is present.
        '''

        if not path.isfile("IPDatabase.db"):
            self._database = None
        else:
            self._database = IPDatabase("IPDatabase.db")


##############################################################################
###### Public Methods ########################################################
##############################################################################

    def start(self) -> None:
        '''
        Description:
            Initiates the programm. Selects between options given by the users arguments.
        '''

        arguments = self._parse_args()

        if arguments.addhosts:
            iplist = arguments.addhosts
            self._add_hosts(iplist)
        
        elif arguments.addhostsbyfile:
            try:
                iplist = self._parse_file(arguments.addhostsbyfile)
            except FileNotFoundError:
                print(("IPDatabase: Error: The file '{}' " + \
                    "could not be found.").format(arguments.addhostsbyfile))
            else:
                self._add_hosts(iplist)      

        elif arguments.allow:
            iplist = arguments.allow
            self._flag(iplist, 1)

        elif arguments.allowbyfile:
            try:
                iplist = self._parse_file(arguments.allowbyfile)
            except FileNotFoundError:
                print(("IPDatabase: Error: The file '{}' " + \
                    "could not be found.").format(arguments.allowbyfile))
            else:
                self._flag(iplist, 1)

        elif arguments.blocklist:
            filename = arguments.blocklist
            self._dump_blocklist(filename)

        elif arguments.deny:
            iplist = arguments.deny
            self._flag(iplist, 0)

        elif arguments.denybyfile:
            try:
                iplist = self._parse_file(arguments.denybyfile)
            except FileNotFoundError:
                print(("IPDatabase: Error: The file '{}' " + \
                    "could not be found.").format(arguments.denybyfile))
            else:
                self._flag(iplist, 0)

        elif arguments.delete:
            iplist = arguments.delete
            self._delete(iplist)

        elif arguments.deletebyfile:
            try:
                iplist = self._parse_file(arguments.deletebyfile)
            except FileNotFoundError:
                print(("IPDatabase: Error: The file '{}'" + \
                    "could not be found.").format(arguments.deletebyfile))
            else:
                self._delete(iplist)

        elif arguments.init:
            self._initiate_database()

        elif arguments.insert:
            iplist = arguments.insert
            self._insert_ips(iplist)

        elif arguments.insertbyfile:
            try:
                iplist = self._parse_file(arguments.insertbyfile)
            except FileNotFoundError:
                print(("IPDatabase: Error: The file '{}' " + \
                    "could not be found.").format(arguments.insertbyfile))
            else:
                self._insert_ips(iplist)

        elif arguments.show:
            self._show_ips()

        elif arguments.pcap:
            if len(arguments.pcap) >= 2 and arguments.pcap[1] == 'addhosts':               
                self._pcap(arguments.pcap[0], True)
            elif len(arguments.pcap) >= 1:
                self._pcap(arguments.pcap[0])
            else:
                print("IPDatabase: Error: argument --pcap: a filename is required.")

        elif arguments.version:
            self._version()

        else:
            print("IPDatabase: Error: expected at least one argument." + 
            " Use --help for more information.")


##############################################################################
###### Private Methods (direct relation) #####################################
##############################################################################


    def _initiate_database(self) -> None:
        '''
        Description:
            Creates the database if it isnt already present.
        '''
        if self._database is None:
            self._database = IPDatabase("IPDatabase.db")
        else:
            print("IPDatabase: Error: The Database already exists.")


    def _flag(self, iplist: list, flagtype:int) -> None:
        '''
        Description:
            Flags the given IPs as allowed or denied.
        
        Parameters:
            iplist: List of IPs to flag
            flagtype: Type of the flag. 1 for allowed, 0 for denied.
        '''
        if self._database is None:
            print("IPDatabase: Error: You must create the database first. " + \
                "Use --help for more information.")
        else:
            flag_errors = self._database.flag_ips(iplist, flagtype)
            self._handle_db_modify_error(flag_errors)


    def _delete(self, iplist:list) -> None:
        '''
        Description: 
            Bulk deletes a list of IPs.
        
        Parameters:
            iplist: List of IPs to deltete
        '''
        if self._database is None:
            print("IPDatabase: Error: You must create the database first. " + \
                "Use --help for more information.")
        else:
            deletion_errors = self._database.delete_ips(iplist)
            self._handle_db_modify_error(deletion_errors)


    def _dump_blocklist(self, filename:str) -> None:
        '''
        Description:
            Dumps a list of IPs to block into a file. 
            IPs will be separated by newlines.
        
        Parameters:
            filename: Name of the file to dump the IPs in.
        '''
        if self._database is None:
            print("IPDatabase: Error: You must create the database first. " + \
                "Use --help for more information.")
        else:
            blocklist_file = open(filename, 'w', encoding='utf8')
            blocklist = self._database.blocklist()
            for ip in blocklist:
                blocklist_file.write("{}\n".format(ip))
            blocklist_file.close()


    def _show_ips(self) -> None:
        '''
        Description:
            Prints stored IPs as a table.
        '''
        if self._database is None:
            print("IPDatabase: Error: You must create the database first. " + \
                "Use --help for more information.")
        else:
            db_ips = self._database.show_ips()
            self._format_table(db_ips)


    def _insert_ips(self, iplist: list) -> None:
        '''
        Description:
            Inserts a list of IPs into the Database. 
        
        Parameters:
            iplist:list of IPs to insert
        '''
        if self._database is None:
            print("IPDatabase: Error: You must create the database first. " + \
                "Use --help for more information.")
        else:
            insertion_errors = self._database.insert_iplist(iplist)
            self._handle_db_insertion_error(insertion_errors)


    def _version(self) -> None:
        '''
        Description:
            Prints the version of the programm.
        '''
        print('IPDatabase: Installed Version: {}'.format(version))


    def _pcap(self, filename:str, addhosts:bool = False) -> None:
        '''
        Description:
            Reads a pcap file and inserts IPs into the database.
            Has an option to also search for corresponding hosts.
        
        Parameters:
            filename: Name of the pcap file
            addhosts: Set this to True if hosts should be searched.
        '''
        if self._database is None:
            print("IPDatabase: Error: You must create the database first. " + \
                "Use --help for more information.")
        elif not path.isfile(filename):
            print("IPDatabase: Error: The given '{}' file could not be found."
                .format(filename))
        else:
            entry_list = self._parse_pcap(filename)
            self._database.insert_iplist(entry_list)
            if addhosts:
                extended_list = self._search_hosts(entry_list)
                self._database.add_hostname(extended_list)


    def _add_hosts(self, iplist: list) -> None:
        '''
        Description:
            Adds the corresponding hostnames of the iplist to the database.
        
        Parameters:
            iplist: List of IPs to search and add hostnames.
        
        Returns:
            return_description
        '''
        if self._database is None:
            print("IPDatabase: Error: You must create the database first. " + \
                "Use --help for more information.")
        else:
            extended_list = self._search_hosts(iplist)
            addhostname_errors = self._database.add_hostname(extended_list)
            self._handle_addhostname_errors(addhostname_errors)


##############################################################################
###### Private Methods #######################################################
##############################################################################  

    def _parse_args(self) -> Namespace:
        '''
        Description:
            parses the Arguments given by the user.
        
        Returns:
            Returns a Namespace object containing all valid arguments 
            and those the user selected.
        '''
        arg_parser = ArgumentParser(description='IPDatabase help-page:')
        self._add_arguments(arg_parser)
        return arg_parser.parse_args()


    def _add_arguments(self, parser:ArgumentParser) -> None:
        '''
        Description:
            Adds valid arguments to the argument-parser.
        
        Parameters:
            parser: Parser of the arguments
        '''
        exclusive_arg_group = parser.add_mutually_exclusive_group()

        valid_exclusive_arguments = [
            ("--addhosts", list, "Searches for hostnames for the given IPs" + \
                "and adds them to the Database. This can take some time."),
            ("--addhostsbyfile", str, "Searches for hostnames for the IPs inside the given file." + \
                "This can take some time. IPs have to be separated by newline."),
            ("--allow", list, "Flags the given sequence of IPs as allowed. " + \
                "Flagged IPs will be marked as UserFlagged."),
            ("--allowbyfile", str, "Flags the sequence of IPs in the given file as allowed. " + \
                "IPs must be separated by newline. Flagged IPs will be marked as UserFlagged."),
            ("--blocklist", str, "Dumps a blocklist into the given file. " + \
                "The entries are separated by newline."),
            ("--deny", list, "Flags the given sequence of IPs as denied. " + \
                "Flagged IPs will be marked as UserFlagged."),
            ("--denybyfile", str, "Flags the sequence of IPs in the given file as denied. " + \
                "IPs must be separated by newline. Flagged IPs will be marked as UserFlagged."),
            ("--delete", list, "Deletes the given sequence of IPs from the database."), 
            ("--deletebyfile", str, "Deletes the sequence of IPs in the given file. " + \
                "IPs must be separated by newline."),         
            ("--init", None, "Creates a 'IPDatabase.db' and initialises the required tables."),
            ("--insert", list, "Inserts the given sequence of IPs into the database."),
            ("--insertbyfile", str, "Inserts the sequence of IPs in the given file. " + \
                "IPs must be separated by newline."), 
            ("--show", None, "Prints the Database content."),
            ("--pcap", list, "Insert IPs by reading a pcap file. This can take some time " + \
                "if hostnames are also searched. Add 'addhosts' as an argument " + \
                "after the filename if you also want to search for hosts."),
            ("--version", None, "Returns the Version of the programm.")
        ]

        for (argument_identifier, argument_type, helpline) in valid_exclusive_arguments:
            if argument_type == None:
                exclusive_arg_group.add_argument(argument_identifier,
                                                help=helpline, action='store_true')
            elif argument_type == str:
                exclusive_arg_group.add_argument(argument_identifier, 
                                                type=argument_type , help=helpline)
            elif argument_type == list:
                exclusive_arg_group.add_argument(argument_identifier, 
                                                nargs='+', type=str , help=helpline)


    def _parse_file(self, filename:str) -> list:
        '''
        Description:
            Parses a file and returns the stored IPs inside the file as a list.
        
        Parameters:
            filename: Name of the file to parse.
        
        Returns:
            List of the IPs stored in the file.

        Exceptions:
            FileNotFoundError: If the given file was not found
        '''
        try:
            file = open(filename, 'r', encoding='utf-8')
        except FileNotFoundError:
            raise FileNotFoundError("The File could not be found.")
        else:
            output_list = []
            file_lines = file.readlines()
            for line in file_lines:
                stripped_line = line.strip()
                output_list.append(stripped_line)
            return output_list

    
    def _handle_db_modify_error(self, errordict:dict) -> None:
        '''
        Description:
            Parses the errordict and prints errors if present.
        
        Parameters:
            errordict: Dict of errors to print.
        '''
        ip_not_found_list = errordict["ip_not_found"]
        invalid_ips = errordict["invalid_ip"]

        for ip in ip_not_found_list:
            print(("IPDatabase: Warning: The IP '{}' " + \
                "could not be found inside the database.").format(ip))
        for ip in invalid_ips:
            print(("IPDatabase: Warning: The Entry '{}' " + \
                "is not a valid IP.").format(ip))


    def _handle_db_insertion_error(self, errordict:dict) -> None:
        '''
        Description:
            Parses the errordict and prints errors if present.
        
        Parameters:
            errordict: Dict of errors to print.
        '''
        duplicates_in_iplist = errordict["duplicates_in_iplist"]
        exists_in_database = errordict["exists_in_database"]
        invalid_ips = errordict["invalid_ip"]

        for ip in duplicates_in_iplist:
            print(("IPDatabase: Warning: The IP '{}' is a duplicate " + \
                "in the insertion list /file.").format(ip))
        for ip in exists_in_database:
            print(("IPDatabase: Warning: The IP '{}' already exists " + \
                "inside the database.").format(ip))
        for ip in invalid_ips:
            print("IPDatabase: Warning: The Entry '{}' is not a valid IP.".format(ip))


    def _handle_addhostname_errors(self, errordict:dict) -> None:
        '''
        Description:
            Parses the errordict and prints errors if present.
        
        Parameters:
            errordict: Dict of errors to print.
        '''
        invalid_ips = errordict["invalid_ip"]
        ip_not_found = errordict["ip_not_found"]

        for ip in invalid_ips:
            print("IPDatabase: Warning: The Entry '{}' is not a valid IP.".format(ip))
        for ip in ip_not_found:
            print("IPDatabase: Warning: The IP '{}' could not be found.".format(ip))
    
    
    def _format_table(self, iptable:list) -> None:
        '''
        Description:
            Formats and prints the IPs to show as a table.
        
        Parameters:
            iplist: IPs to show in a table
        '''
        if iptable != []:
            print("+{:<8}+{:<40}+{:<9}+{:<7}+{:<12}+{:<16}"
                .format('-' * 8, '-' * 40, '-' * 9, '-' * 7, '-' * 12, '-' * 16))
            print("| {:<7}| {:<39}| {:<8}| {:<6}| {:<11}| {}"
                .format('ID', 'IP', 'Version', 'Flag', 'User Edited','Hostname'))
            print("+{:<8}+{:<40}+{:<9}+{:<7}+{:<12}+{:<16}"
                .format('-' * 8, '-' * 40, '-' * 9, '-' * 7, '-' * 12, '-' * 16))

            for (rowid, ip, iptype, flag, userflagged, hostname) in iptable:
                if flag == 1:
                    print_flag = 'allow'
                elif flag == 0:
                    print_flag = 'deny'
                if userflagged == 1:
                    print_userflagged = 'Yes'
                elif userflagged == 0:
                    print_userflagged = 'No'
                if hostname is None:
                    print_hostname = ''
                else:
                    print_hostname = hostname
                print("| {:<7}| {:<39}| {:<8}| {:<6}| {:<11}| {}"
                    .format(rowid, ip, iptype, print_flag, print_userflagged, print_hostname))
            print("+{:<8}+{:<40}+{:<9}+{:<7}+{:<12}+{:<16}"
                .format('-' * 8, '-' * 40, '-' * 9, '-' * 7, '-' * 12, '-' * 16))        
        print("IPDatabase: total entries: {}".format(len(iptable)))


    def _parse_pcap(self, filename:str) -> list:
        '''
        Description:
            Parses a pcap file and returns the stored IPs inside the file as a list.
        
        Parameters:
            filename: Name of the pcap file to parse.
        
        Returns:
            List of the IPs stored in the file.

        Exceptions:
            FileNotFoundError: If the given file was not found
        '''
        if not path.isfile(filename):
            raise FileNotFoundError('The given file could not be found.')
        ips = set()
        
        # open the pcap file
        with open(filename, 'rb') as pcap_file:

            pcap_input = pcap.Reader(pcap_file)

            # iterate over every ehternet-frame
            for (_, frame) in pcap_input:
                eth_frame = ethernet.Ethernet(frame)

                # Type in the ethernet header = 0x0800
                if eth_frame.type == ethernet.ETH_TYPE_IP:
                    ip_packet = eth_frame.data
                    # Read source and destination IP from IP-header
                    src_ip = inet_ntop(AF_INET, ip_packet.src)
                    dst_ip = inet_ntop(AF_INET, ip_packet.dst)
                    ips.add(src_ip)
                    ips.add(dst_ip)

                # Txpe in the ethernet header = 0x86dd
                elif eth_frame.type == ethernet.ETH_TYPE_IP6:
                    ip_packet = eth_frame.data
                    # Read source and destination IP from IP-header
                    src_ip = inet_ntop(AF_INET6, ip_packet.src)
                    dst_ip = inet_ntop(AF_INET6, ip_packet.dst)
                    ips.add(src_ip)
                    ips.add(dst_ip)

        return list(ips)


    def _validate_entry(self, entry:str) -> tuple:
        '''
        Description:
            Validates an entry, searches the hostname for an ip and vice versa
        
        Parameters:
            entry: The entry to validate
        
        Returns:
            Tuple containing (ip, hostname)
        '''
        try: 
            (hostname, _ , _) = gethostbyaddr(entry)
        except herror:
            return (entry, None)
        except gaierror:
            return (entry, None)
        else:
            return (entry, hostname)


    def _validate_ip(self, ip:str) -> int:
        '''
        Description: 
            Checks wheter the the given IP is valid 
        
        Parameters:
            ip: the IP to check
        
        Returns:
            Returns the ip Version(4 or 6)

        Exceptions: 
            Raises an AddrFormatError if IP is invalid
        '''
        try:
            return IPAddress(ip).version
        except AddrFormatError:
            raise AddrFormatError("The given IP is not valid.") 


    def _search_hosts(self, iplist:list) -> list:
        '''
        Description:
            Adds hosts to the given IPlist

        Parameters:
            iplist: list of IPs to search hosts for
        
        Returns:
            Returns a list of tuples with the following scheme:
            [(ip0, host0), (ip1, host1), ...]
        '''
        no_entries = len(iplist)
        # limit the max amount of threads to 64 to prevent
        # too many files from being opened
        if no_entries > 64:
            no_entries = 64
        with Pool(no_entries) as p:
            hostlist = p.map(self._validate_entry, iplist)
        return hostlist


##############################################################################
###### Main Programm #########################################################
##############################################################################     

if __name__ == '__main__':
    version = "2.1.0"
    ui = Interface()
    ui.start()
