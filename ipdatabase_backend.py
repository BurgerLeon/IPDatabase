# -*- coding: utf-8 -*-
'''
Filename: ipdatabase.py
Author: Leon Burger
Date: 29.12.2020
Python Version: 3.9.1
Dependencies: 
    sqlite3 -> provides the main database to store the IPs
    os -> provides access to system filesystem
    netaddr -> provides network address manipulation
'''

from sqlite3 import connect, Connection
from os import path
from netaddr import IPAddress, AddrFormatError

class IPDatabase:
    '''
    Description: 
        Represents the main Database to store the IPs. Supports IPv4 and IPv6. 
    
    Parameters:
        filename:(str) Name of the database file (without ending)

    Methods:
        insert_iplist(list): Bulk insert a list of IPs into the database
        flag_ips(list, bool): Bulk flag IPs inside the database. Flagtype
            True for allowed, False for rejected
        delete_ips(list): Bulk delete IPs inside the Database
        blocklist(): Exports the blocked IPs into a textfile seperated by linefeed
        show_ips(): Return the IPs inside a tuple including metadata, can be filtered
    '''

    def __init__(self, filename:str) -> None: 
        '''
        Description: 
            Creates a local database storing the IPs, their type and their flag.
        
        Parameters:
            iplist: Includes the initial IPs for the Database (empty by default)
        '''
        self._filename = filename
        #Check if database already exists
        if not path.isfile(self._filename):
            self._initiate_ipdatabase()


###############################################################################
### Public Methods ############################################################
###############################################################################

    def insert_iplist(self, iplist:list) -> dict:
        '''
        Description:
            Inserts a list of IPs into the Database. 
        
        Parameters:
            iplist:list of IPs to insert
        
        Returns:
            returns a dict with the keys: 
            {"duplicates_in_iplist", "exists_in_database", "invalid_ip"}
            Each key refers a list containing entries that caused that error.
        '''

        insertion_errors = {
            "duplicates_in_iplist": [],
            "exists_in_database": [],
            "invalid_ip": []
        }

        # insert into ipcache (without duplicates)
        cache_insertion_errors = self._insert_ips_cache(iplist)
        insertion_errors["duplicates_in_iplist"] = cache_insertion_errors["duplicates_in_iplist"]

        # get cached IPs
        cached_ips = self._extract_first_fetch_value(self._get_cache())


        # insert cached IPs into iptable (without duplicates)
        iptable_insertion_errors = self._insert_ips_iptable(cached_ips)
        insertion_errors["invalid_ip"] = iptable_insertion_errors["invalid_ip"]
        insertion_errors["exists_in_database"] = iptable_insertion_errors["exists_in_database"]

        # wipe cache
        self._wipe_cache()

        return insertion_errors


    def flag_ips(self, iplist:list, flagtype:int) -> dict:
        '''
        Description: 
            Bulk flags a list of IPs. IPs flagged via this 
            will be marked as UserFlagged.
        
        Parameters:
            iplist: List of IPs to flag
            flagtype: Type of the flag. 1 for allowed, 
                0 for denied.
        
        Returns:
            returns a dict with the keys: 
            {"ip_not_found", "invalid_ip"}
            Each key refers a list containing entries that caused that error.

        Exceptions:
            Raises a ValueError if flagtype is not 0 or 1.
        '''
        if not(flagtype == 0 or flagtype == 1):
            raise ValueError("Flagtype must be 0 or 1.")
        flag_errors = self._flag_ips(iplist, flagtype)
        return flag_errors


    def delete_ips(self, iplist:list) -> dict:
        '''
        Description: 
            Bulk deletes a list of IPs.
        
        Parameters:
            iplist: List of IPs to deltete
        
        Returns:
            returns a dict with the keys: 
            {"ip_not_found", "invalid_ip"}
            Each key refers a list containing entries that caused that error.
        '''
        deletion_errors = self._delete_ips(iplist)
        return deletion_errors


    def blocklist(self) -> list:
        '''
        Description: 
            Returns a list of IPs, that a flagged as denied.
        
        Returns:
            List of IPs, that a flagged as denied.
        '''
        blocklist = self._extract_first_fetch_value(self._get_blocklist())
        return blocklist


    def show_ips(self) -> list:
        '''
        Description:
            Returns a list containing all IPs with corresponding 
            ID, Flag, Type and UserFlagged status.
        
        Returns:
            List containing all IPs with corresponding 
            ID, Flag, Type and UserFlagged status.
        '''
        ips = self._get_ip_entries()
        return ips


    def add_hostname(self, entrylist:list) -> dict:
        '''
        Description:
            adds corresponding hostnames to the given IPs
        
        Parameters:
            entrylist: list of tuples containing ips and hostnames
                    [(ip0, hostname0), (ip1, hostname1), ...]

        Returns:
            returns a dict with the keys: 
            {"ip_not_found", "invalid_ip"}
            Each key refers a list containing entries that caused that error.

        Exceptions:
            ValueError: If the list does not consist of tuples with length 2
        '''
        if not all([len(elem) == 2 for elem in entrylist]):
            raise ValueError("All tuples of the entrylist must be pairs \
                of IPs and hostnames")
        add_hostname_errors = self._add_hostname(entrylist)
        return add_hostname_errors
    
    
###############################################################################
### Private Methods ###########################################################
###############################################################################

    def _initiate_ipdatabase(self) -> None:
        '''
        Description:
            Creates the database with a main table containing:
        '''
        
        #Create the database and connect to it
        dbconnection = connect(self._filename)
        self._create_tables(dbconnection)
        self._insert_valid_ip_types(dbconnection)
        dbconnection.commit()


    def _create_tables(self, conn:Connection) -> None:
        '''
        Description:
            Initialises the Main Types and Flags table
        
        Parameters:
            conn: Connection to the database
        '''

        sql_create_table_types = '''
            CREATE TABLE Types(
                Type TEXT UNIQUE NOT NULL
            );
        '''
        conn.execute(sql_create_table_types)

        #Flag = 1 is allowed | Flag = 0 is rejected
        #UserFlagged: 1 is True, 0 is Flase
        sql_create_table_main_iptable = '''
            CREATE TABLE IPTable(
                IP TEXT UNIQUE NOT NULL,
                Type INTEGER NOT NULL,
                Flag INTEGER NOT NULL,
                UserFlagged INTEGER NOT NULL,
                Hostname TEXT,
                FOREIGN KEY(Type) REFERENCES Types(rowid)
            );
        '''
        conn.execute(sql_create_table_main_iptable)

        sql_create_table_ipcache = '''
            CREATE TABLE IPCache(
                IP TEXT UNIQUE NOT NULL
            );
        '''
        conn.execute(sql_create_table_ipcache)


    def _insert_valid_ip_types(self, conn:Connection) -> None:
        '''
        Description: 
            Inserts the types into to Types table
        
        Parameters:
            conn: Connection to the database
        '''

        for iptype in (4, 6):
            sql_insert_valid_ip_types = '''
                INSERT INTO Types(Type)
                VALUES({});
            '''.format(iptype)
            conn.execute(sql_insert_valid_ip_types)


    def _validate_ip(self, ip:str) -> int:
        '''
        Description: 
            Checks wheter the the given IP is valid 
        
        Parameters:
            ip: the IP to check
        
        Returns:
            Returns the ip Version(4 or 6)

        Exceptions: 
            Raises an AddrFormatError if IP is invalid
        '''

        try:
            return IPAddress(ip).version
        except AddrFormatError:
            raise AddrFormatError("The given IP is not valid.") 


    def _insert_ips_iptable(self, iplist:list) -> dict:
        '''
        Description: 
            Bulk inserts a list of IPs into IPtable
        
        Parameters:
            iplist: list of IPs to insert
        
        Returns:
            returns a dict with the keys: 
            {"exists_in_database", "invalid_ip"}
            Each key refers a list containing entries that caused that error.
        '''
        insertion_errors = {
            "invalid_ip": [],
            "exists_in_database": []
        }

        dbconnection = connect(self._filename)
        for ip in iplist:
            try:
                ip_version = self._validate_ip(ip)
            except AddrFormatError:
                insertion_errors["invalid_ip"].append(ip)
            else:        
                insert_successful = self._insert_ip_into_iptable(ip, ip_version, dbconnection)
                if not insert_successful:
                    insertion_errors["exists_in_database"].append(ip)
        dbconnection.commit()
        return insertion_errors


    def _insert_ip_into_iptable(self, ip:str, ip_version:int, conn:Connection) -> bool:
        '''
        Description: 
            Insert an IP into the IPtable table.
            (if you dont have a connection call _insert_ips_iptable instead)
        
        Parameters:
            ip: IP to insert
            ip_version: version of the IP
            conn: Connection to the database
        
        Returns:
            True if IP was successfully inserted, False if the IP already exists.
        '''
           
        if not self._lookup_ip(ip, 'IPTable', conn):
            #Insert an IP (Flag = 1 (allowed), 
            # UserFlagged = False (not flagged by the user yet) by default)
            sql_insert_ip = '''
                INSERT INTO IPTable(IP, Type, Flag, UserFlagged, Hostname)
                VALUES(?, ?, 1, 0, NULL);
            '''
            conn.execute(sql_insert_ip, (ip, ip_version))
            return True
        else:
            return False


    def _insert_ips_cache(self, iplist:list) -> dict:
        '''
        Description: 
            Bulk insert a list of IPs into the cache table.
            IPs will NOT be validated.
        
        Parameters:
            iplist: list of IPs to insert
        
        Returns:
            returns a dict with the keys: 
            {"duplicates_in_iplist"}
            Each key refers a list containing entries that caused that error.
        '''
        insertion_errors = {
            "duplicates_in_iplist": []
        }

        dbconnection = connect(self._filename)
        for ip in iplist:
            insertion_successful = self._insert_ip_into_cache(ip, dbconnection)
            if not insertion_successful:
                insertion_errors["duplicates_in_iplist"].append(ip)
        dbconnection.commit()
        return insertion_errors
        

    def _insert_ip_into_cache(self, ip:str, conn:Connection) -> bool:
        '''
        Description: 
            Insert an IP into the IPcache table.
            (if you dont have a connection call _insert_ips_cache instead)
        
        Parameters:
            ip: IP to insert
            conn: Connection to the database
        
        Returns:
            True if IP was successfully inserted, False if the IP already exists.
        '''
        if not self._lookup_ip(ip, 'IPCache', conn):
            #Insert a IP 
            sql_insert_ip = '''
                INSERT INTO IPCache(IP)
                VALUES(?);
            '''
            conn.execute(sql_insert_ip, (ip, ))
            return True
        else:
            return False


    def _lookup_ip(self, ip:str, table:str, conn:Connection) -> bool:
        '''
        Description: 
            looks up an IP in a table
        
        Parameters:
            ip: The IP to search for
            table: The table to search in
            conn: Connection to the database
        
        Returns:
            True if found, False if not found
        '''

        dbcursor = conn.cursor()

        sql_select_ip = '''
            SELECT IP 
            FROM {} 
            WHERE IP = ?;
        '''.format(table)
        dbcursor.execute(sql_select_ip, (ip, ))
        #fetchall will be an empty list if ip was not found
        return not(dbcursor.fetchall() == [])


    def _get_cache(self) -> list:
        '''
        Description: 
            returns the cache of the database
        
        Returns:
            cachedata stored in a list
        '''
        dbconnection = connect(self._filename)
        return self._get_ips_from_cache_table(dbconnection)


    def _get_ips_from_cache_table(self, conn:Connection) -> list:
        '''
        Description: 
            returns the IPs from the cache table
        
        Parameters:
            conn: Connection to the database (if you dont have a connection to the database,
                call _get_cache() instead)
        
        Returns:
            IPs of the cachetable stored in a list
        '''

        dbcursor = conn.cursor()
        sql_select_ip = '''
            SELECT IP 
            FROM IPCache; 
        '''
        dbcursor.execute(sql_select_ip)
        return dbcursor.fetchall()


    def _extract_first_fetch_value(self, fetchlist:list) -> list:
        '''
        Description: Extracts the first value out of each tuple:
            Example: [('A', ...), ('B', ...), ('C', ...)]  
                     results in: ['A', 'B', 'C']
        
        Parameters:
            fetchlist:list of tuples
        
        Returns:
            List of the first value of each tuple
        '''
        iplist = [None] * len(fetchlist)
        for (index, (ip, )) in enumerate(fetchlist):
            iplist[index] = ip
        return iplist


    def _wipe_cache(self) -> None:
        '''
        Description: 
            deletes all data stored in the database cache
        '''
        dbconnection = connect(self._filename) 
        self._delete_cached_entries_from_cache_table(dbconnection)
        dbconnection.commit()


    def _delete_cached_entries_from_cache_table(self, conn:Connection) -> None:
        '''
        Description: 
            Deletes all stored IPs in the IPcache table.

        Parameters:
            conn: Connection to the database (if you dont have a connection to the database,
                call _wipe_cache() instead)
        '''
        sql_wipe_cache = '''
            DELETE FROM IPCache;
        '''
        conn.execute(sql_wipe_cache)


    def _flag_ips(self, iplist:list, flagtype:int) -> dict:
        '''
        Description: 
            Bulk flags a list of IPs. IPs flagged via this 
            will be marked as UserFlagged.
        
        Parameters:
            iplist: List of IPs to flag
            flagtype: Type of the flag. 1 for allowed, 
                0 for denied.
        
        Returns:
        returns a dict with the keys: 
            {"ip_not_found", "invalid_ip"}
            Each key refers a list containing entries that caused that error.
        '''
        flag_errors = {
            "invalid_ip": [],
            "ip_not_found": []
        }

        dbconnection = connect(self._filename)
        for ip in iplist:
            try:
                self._validate_ip(ip)
            except AddrFormatError:
                flag_errors["invalid_ip"].append(ip)
            else:
                if self._lookup_ip(ip, 'IPTable', dbconnection):
                    self._mark_ip_as_flagged(ip, flagtype, dbconnection)
                else:
                    flag_errors["ip_not_found"].append(ip)
        dbconnection.commit()
        return flag_errors


    def _mark_ip_as_flagged(self, ip:str, flagtype:int, conn:Connection) -> None:
        '''
        Description: 
            Updates flag of the IP-entry. 
        
        Parameters:
            ip: IP to flag
            flagtype: Type of the flag. 1 for allowed, 
                0 for denied.
            conn: Connection to the database       
        '''
        flag_ip_sql = '''
            UPDATE IPTable
            SET Flag = ?,
                UserFlagged = 1
            WHERE IP = ?;
        '''
        conn.execute(flag_ip_sql, (flagtype, ip))


    def _delete_ips(self, iplist:list) -> dict:
        '''
        Description: 
            Bulk deletes a list of IPs.
        
        Parameters:
            iplist: List of IPs to deltete
        
        Returns:
        returns a dict with the keys: 
            {"ip_not_found", "invalid_ip"}
            Each key refers a list containing entries that caused that error.
        '''
        deletion_errors = {
            "invalid_ip": [],
            "ip_not_found": []
        }

        dbconnection = connect(self._filename)
        for ip in iplist:
            try:
                self._validate_ip(ip)
            except AddrFormatError:
                deletion_errors["invalid_ip"].append(ip)
            else:
                if self._lookup_ip(ip, 'IPTable', dbconnection):
                    self._remove_ip_from_iptable(ip, dbconnection)
                else:
                    deletion_errors["ip_not_found"].append(ip)
        dbconnection.commit()
        return deletion_errors


    def _remove_ip_from_iptable(self, ip:str, conn:Connection) -> None:
        '''
        Description: 
            Updates flag of the IP-entry. 
        
        Parameters:
            ip: IP to flag
            flagtype: Type of the flag. True for allowed, 
                False for denied.
            conn: Connection to the database       
        '''
        flag_ip_sql = '''
            DELETE FROM IPTable
            WHERE IP = ?;
        '''
        conn.execute(flag_ip_sql, (ip, ))


    def _get_blocklist(self) -> list:
        '''
        Description: 
            Returns a list of IPs, that a flagged as denied.
        
        Returns:
            List of IPs, that a flagged as denied.
        '''
        dbconnection = connect(self._filename)
        dbcursor = dbconnection.cursor()
        sql_select_ip = '''
            SELECT IP 
            FROM IPTable 
            WHERE Flag = 0;
        '''
        dbcursor.execute(sql_select_ip)
        return dbcursor.fetchall()


    def _get_ip_entries(self) -> list:
        '''
        Description: 
            Returns a list containing all IPs with corresponding 
            ID, Flag, Type and UserFlagged status.
        
        Returns:
            List containing all IPs with corresponding 
            ID, Flag, Type and UserFlagged status.
        '''
        dbconnection = connect(self._filename)
        dbcursor = dbconnection.cursor()
        sql_select_ip = '''
            SELECT rowid, IP, Type, Flag, UserFlagged, Hostname
            FROM IPTable;
        '''
        dbcursor.execute(sql_select_ip)
        return dbcursor.fetchall()


    def _add_hostname(self, entrylist) -> dict:
        '''
        Description:
            Adds the given hostname to the IP
        
        Parameters:
            ip: The IP to add the hostname onto
            hostname: The hostname to add

        Returns:
            returns a dict with the keys: 
            {"ip_not_found", "invalid_ip"}
            Each key refers a list containing entries that caused that error.
        '''
        add_hostname_errors = {
            "invalid_ip": [],
            "ip_not_found": []
        }
        dbconnection = connect(self._filename)
        for (ip, hostname) in entrylist:
            try:
                self._validate_ip(ip)
            except AddrFormatError:
                add_hostname_errors["invalid_ip"].append(ip)
            else:
                if self._lookup_ip(ip, 'IPTable', dbconnection):
                    if hostname != None:
                        self._add_hostname_in_table(ip, hostname, dbconnection)
                else:
                    add_hostname_errors["ip_not_found"].append(ip)
        dbconnection.commit()
        return add_hostname_errors

        
    def _add_hostname_in_table(self, ip:str, hostname:str, conn:Connection) -> None:
        '''
        Description:
            Appends the given hostname to the IP inside the Table 
        
        Parameters:
            ip: The IP to add the hostname onto
            hostname: The hostname to add
            conn: The connection to the database
        '''
        flag_ip_sql = '''
            UPDATE IPTable
            SET Hostname = ?
            WHERE IP = ?;
        '''
        conn.execute(flag_ip_sql, (hostname, ip))
    
    